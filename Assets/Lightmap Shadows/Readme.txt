Dynamic Shadows for Lightmapped surface version 1.1
Proof of Concept

Author: Laurens Mathot, Code Animo

"Shadow without light"
Written for use with Unity version 4.2. Designed for the Deferred Rendering Path, with lightmaps.
If the effect doesn�t show up after first importing it, you may need to restart the Unity editor.

The 'DynamicShadowLightmapped' shader can be used in a material for any object that should apply the shadow received from a spotlight to the lighting it receives from its lightmap, without applying the actual lighting of that spotlight. You can adjust the influence of the shadow using the �strength� slider of the spotlight.

This plugin replaces the internal Lighting Pass (Internal-PrePassLighting.shader) to pack shadow data in the alpha channel. Unlike other shaders, this might not automatically 
be detected by Unity until the editor is restarted.

There's no need to switch to different materials when baking lightmaps. Beast will automatically read the data it needs from the shader properties (More information about this can be found on the 'LightMapping In Depth' guide in the Unity Documentation, under the header 'Material Properties' http://docs.unity3d.com/Documentation/Manual/LightmappingInDepth.html ). Not all those properties are set in this version of the shader, but they shouldn't be very difficult to add.

In this version, Directional Lightmaps and the Forward Rendering path are not supported. Also, the surfaces using this material dont cast shadows by default :(

By request, the PSP2 renderer is ignored. Unfortunately, this renderer is not yet recognized in Unity version 4.2, so it will show a warning.
