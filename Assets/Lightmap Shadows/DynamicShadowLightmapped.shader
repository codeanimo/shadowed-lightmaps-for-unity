Shader "Zoink/DynamicShadowLightmapped" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass{
			Tags { "LightMode" = "PrePassBase" }
			Fog {Mode Off}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers psp2
			#define UNITY_PASS_PREPASSBASE
			#include "UnityCG.cginc"
			
			
			void vert (appdata_base v, out fixed3 normal:TEXCOORD0, out float4 pos:POSITION){
				pos = mul (UNITY_MATRIX_MVP, v.vertex);
				normal = mul((float3x3)_Object2World, SCALED_NORMAL);
			}
	
			fixed4 frag(fixed3 normal:TEXCOORD0):COLOR{				
				fixed4 outputData;
				
				outputData.rgb = normal * 0.5 + 0.5;
				outputData.a = 0.0f;//Specular
				
				return outputData;
			}
			ENDCG
			
		}
		Pass{
		
			Tags { "LightMode" = "PrePassFinal" }
			ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers psp2
			#pragma multi_compile_prepassfinal
			#define UNITY_PASS_PREPASSFINAL
			#include "UnityCG.cginc"
			
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			#ifndef LIGHTMAP_OFF
			sampler2D unity_Lightmap;
//			#ifndef DIRLIGHTMAP_OFF
//			sampler2D unity_LightmapInd;
//			#endif
			#endif			
			float4 unity_LightmapST;
			
			sampler2D _LightBuffer;
			
			void vert (appdata_full v, out float4 screen:TEXCOORD0, out float4 texcoord1 : TEXCOORD1,out float4 pos:POSITION){
				pos = mul(UNITY_MATRIX_MVP,v.vertex);
				texcoord1.xy = TRANSFORM_TEX (v.texcoord, _MainTex);
				
				screen = ComputeScreenPos (pos);
				texcoord1.zw = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				
			}
	
			float4 frag(float4 screen:TEXCOORD0, float4 texcoord1:TEXCOORD1):COLOR{
				float4 outputColor = tex2D(_MainTex, texcoord1.xy);
											
				#ifndef LIGHTMAP_OFF
				#ifdef DIRLIGHTMAP_OFF
				float4 lmtex = tex2D(unity_Lightmap, texcoord1.zw);
				float3 lightMap = DecodeLightmap (lmtex);
				#else
				// Directional lightmaps not yet supported:
				float3 lightMap = float3(1,0,1);
//				fixed4 lmIndTex = tex2D(unity_LightmapInd, IN.lmap.xy);
//				half3 lm = LightingLambert_DirLightmap(o, lmtex, lmIndTex, 0).rgb;
				#endif
								
				#endif
				
				float4 light = tex2Dproj (_LightBuffer, UNITY_PROJ_COORD(screen));
				#ifndef HDR_LIGHT_PREPASS_ON
					light = -log2(light);
				#endif
				
				#ifndef LIGHTMAP_OFF
				outputColor.rgb *= lightMap * (1-light.w);
				#else
				outputColor.rgb *= float3(1,1,0);
				#endif
				
				return outputColor;
			}
			ENDCG
		}
	}
}
